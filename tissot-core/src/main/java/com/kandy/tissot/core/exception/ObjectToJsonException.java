package com.kandy.tissot.core.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 对象转换成json异常类
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ObjectToJsonException extends RuntimeException{

    private static final String OBJECT_TO_JSON_EXCEPTION_MSG = "对象转换成JSON异常";

    public ObjectToJsonException(Throwable throwable){
        super(OBJECT_TO_JSON_EXCEPTION_MSG,throwable);
    }
}
