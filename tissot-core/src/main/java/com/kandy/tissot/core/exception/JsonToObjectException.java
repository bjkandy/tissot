package com.kandy.tissot.core.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * json转换成对象异常类
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class JsonToObjectException extends RuntimeException {

    private static final String JSON_TO_OBJECT_EXCEPTION_MSG = "JSON转换成对象异常";

    public JsonToObjectException(Throwable throwable){
        super(JSON_TO_OBJECT_EXCEPTION_MSG,throwable);
    }
}

